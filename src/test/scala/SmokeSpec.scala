import com.core.SparkSessionTestWrapper
import org.apache.spark.sql.DataFrame
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

import scala.collection.mutable

class SmokeSpec extends AnyFlatSpec with should.Matchers with SparkSessionTestWrapper {

  "A SmokeTest" should "Fire up some shit" in {
    true shouldNot be(false)
  }

  "A SparkTest" should "Build some Data" in {
    import spark.implicits._
    val df: DataFrame = Seq((1, 2), (2, 3)).toDF("a", "b")

    df.count should be(2)
    df.columns.sorted shouldEqual List("a", "b").sorted
  }

  "A Stack" should "pop values in last-in-first-out order" in {
    val stack = new mutable.Stack[Int]
    stack.push(1)
    stack.push(2)
    stack.pop() should be(2)
    stack.pop() should be(1)
  }

  it should "throw NoSuchElementException if an empty stack is popped" in {
    val emptyStack = new mutable.Stack[Int]
    a[NoSuchElementException] should be thrownBy {
      emptyStack.pop()
    }
  }
}