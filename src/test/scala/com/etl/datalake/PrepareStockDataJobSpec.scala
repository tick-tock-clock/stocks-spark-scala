package com.etl.datalake

import com.core.SparkSessionTestWrapper
import org.apache.spark.sql.DataFrame
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should

class PrepareStockDataJobSpec extends AnyFunSpec with should.Matchers with SparkSessionTestWrapper {

  import spark.implicits._

  val rawStocks: DataFrame = Seq(
    ("2021-01-04", "AAPL", 15.0, 30.0), // expected to disappear
    ("2021-01-04", "GOOG", 115.0, 130.0), // expected to disappear
    ("2021-01-05", "AAPL", 16.0, 31.0), // expected to disappear
    ("2021-01-05", "GOOG", 116.0, 131.0), // expected to disappear
    ("2021-01-06", "AAPL", 17.0, 32.0),
    ("2021-01-06", "GOOG", 117.0, 132.0),
    ("2021-01-07", "AAPL", 18.0, 33.0),
    ("2021-01-07", "GOOG", 118.0, 133.0),
    ("2021-01-08", "AAPL", 19.0, 34.0),
    ("2021-01-08", "GOOG", 119.0, 134.0),
    ("2021-01-11", "AAPL", 20.0, 35.0),
    ("2021-01-11", "GOOG", 120.0, 135.0),
    ("2021-01-12", "AAPL", 21.0, 36.0),
    ("2021-01-12", "GOOG", 121.0, 136.0),
    ("2021-01-13", "AAPL", 22.0, 37.0), // expected to disappear
    ("2021-01-13", "GOOG", 122.0, 137.0), // expected to disappear
    ("2021-01-14", "AAPL", 23.0, 38.0), // expected to disappear
    ("2021-01-14", "GOOG", 123.0, 138.0) // expected to disappear
  ).toDF("dt", "ticker", "close", "high")


  describe("shiftPricesAndAppendAsColumn") {

    val expectedOutput: DataFrame = Seq(
      ("2021-01-06", "AAPL", 32.0, 15.0, 16.0, 17.0, 18.0, 19.0),
      ("2021-01-06", "GOOG", 132.0, 115.0, 116.0, 117.0, 118.0, 119.0)
    ).toDF("dt", "ticker", "high", "close-2", "close-1", "close", "close+1", "close+2")
      .cache()

    val actualOutput: DataFrame = PrepareStockDataJob
      .shiftPricesAndAppendAsColumn(rawStocks, "close").cache()

    it("should return the expected columns") {
      actualOutput.columns.sorted shouldEqual expectedOutput.columns.sorted
    }

    it("should have the expected count") {
      actualOutput.count shouldEqual 18
    }

    it("should return null where window is exceeded") {
      val filteredOutputFirst = actualOutput
        .filter($"dt" === "2021-01-04" && $"ticker" === "AAPL").cache()

      assert(filteredOutputFirst.isNull("close-2"))
      assert(filteredOutputFirst.isNull("close-1"))

      val filteredOutputEnd = actualOutput
        .filter($"dt" === "2021-01-14" && $"ticker" === "AAPL").cache()

      assert(filteredOutputEnd.isNull("close+1"))
      assert(filteredOutputEnd.isNull("close+2"))

    }

    it("should produce the expected shifted dataframe") {
      val filteredOutputApple = actualOutput
        .filter($"dt" === "2021-01-06" && $"ticker" === "AAPL").cache()
      val expectedOutputApple = expectedOutput
        .filter($"dt" === "2021-01-06" && $"ticker" === "AAPL").cache()

      filteredOutputApple.getDouble("close-2") shouldEqual expectedOutputApple.getDouble("close-2")
      filteredOutputApple.getDouble("close-1") shouldEqual expectedOutputApple.getDouble("close-1")
      filteredOutputApple.getDouble("close") shouldEqual expectedOutputApple.getDouble("close")
      filteredOutputApple.getDouble("close+1") shouldEqual expectedOutputApple.getDouble("close+1")
      filteredOutputApple.getDouble("close+2") shouldEqual expectedOutputApple.getDouble("close+2")

      val filteredOutputGoogle = actualOutput
        .filter($"dt" === "2021-01-06" && $"ticker" === "GOOG").cache()
      val expectedOutputGoogle = expectedOutput
        .filter($"dt" === "2021-01-06" && $"ticker" === "GOOG").cache()

      filteredOutputGoogle.getDouble("close-2") shouldEqual expectedOutputGoogle.getDouble("close-2")
      filteredOutputGoogle.getDouble("close-1") shouldEqual expectedOutputGoogle.getDouble("close-1")
      filteredOutputGoogle.getDouble("close") shouldEqual expectedOutputGoogle.getDouble("close")
      filteredOutputGoogle.getDouble("close+1") shouldEqual expectedOutputGoogle.getDouble("close+1")
      filteredOutputGoogle.getDouble("close+2") shouldEqual expectedOutputGoogle.getDouble("close+2")

    }

  }

  describe("shiftPricesForKeyColumns") {

    val expectedFilteredOutput: DataFrame = Seq(
      ("2021-01-06", "AAPL", 15.0, 16.0, 17.0, 18.0, 19.0, 30.0, 31.0, 32.0, 33.0, 34.0)
    ).toDF("dt", "ticker",
      "close-2", "close-1", "close", "close+1", "close+2",
      "high-2", "high-1", "high", "high+1", "high+2").cache()

    val actualOutput: DataFrame = PrepareStockDataJob
      .shiftPricesForKeyColumns(rawStocks, "close", "high").cache()

    it("should return the expected columns") {
      actualOutput.columns.sorted shouldEqual expectedFilteredOutput.columns.sorted
    }

    it("should have the expected count") {
      actualOutput.count shouldEqual 10
    }

    it("should produce the expected shifted dataframe") {
      val filteredOutput = actualOutput
        .filter($"dt" === "2021-01-06" && $"ticker" === "AAPL").cache()

      filteredOutput.getDouble("close-2") shouldEqual expectedFilteredOutput.getDouble("close-2")
      filteredOutput.getDouble("close-1") shouldEqual expectedFilteredOutput.getDouble("close-1")
      filteredOutput.getDouble("close") shouldEqual expectedFilteredOutput.getDouble("close")
      filteredOutput.getDouble("close+1") shouldEqual expectedFilteredOutput.getDouble("close+1")
      filteredOutput.getDouble("close+2") shouldEqual expectedFilteredOutput.getDouble("close+2")

      filteredOutput.getDouble("high-2") shouldEqual expectedFilteredOutput.getDouble("high-2")
      filteredOutput.getDouble("high-1") shouldEqual expectedFilteredOutput.getDouble("high-1")
      filteredOutput.getDouble("high") shouldEqual expectedFilteredOutput.getDouble("high")
      filteredOutput.getDouble("high+1") shouldEqual expectedFilteredOutput.getDouble("high+1")
      filteredOutput.getDouble("high+2") shouldEqual expectedFilteredOutput.getDouble("high+2")
    }
  }
}
