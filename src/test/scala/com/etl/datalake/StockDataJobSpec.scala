package com.etl.datalake

import com.core.SparkSessionTestWrapper
import org.apache.spark.sql.DataFrame
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should

class StockDataJobSpec extends AnyFunSpec with should.Matchers with SparkSessionTestWrapper {

  import spark.implicits._

  describe("adjustStockSplit") {

    val input: DataFrame = Seq(
      ("2020-08-26", "FOO", 10000L, 1D, 2000D, 1500D),
      ("2020-08-27", "FOO", 10000L, 1D, 2100D, 1600D),
      ("2020-08-28", "FOO", 10000L, 1D, 2200D, 1700D),
      ("2020-08-31", "FOO", 10000L, 5D, 450D, 340D),
      ("2020-09-01", "FOO", 10000L, 1D, 500D, 400D),
      ("2020-09-02", "FOO", 10000L, 1D, 600D, 500D),
      ("2020-09-03", "FOO", 10000L, 1D, 700D, 600D),
      ("2020-09-04", "FOO", 10000L, 2D, 300D, 150D),
      ("2020-09-05", "FOO", 10000L, 1D, 350D, 200D),
      ("2020-09-06", "FOO", 10000L, 1D, 400D, 250D),
      ("2020-09-07", "FOO", 10000L, 1D, 450D, 300D)
    ).toDF("dt", "ticker", "volume", "split_coefficient", "high", "low")

    val output: DataFrame = input
      .transform(StockDataJob.adjustStockSplit(_, Seq[String]("volume", "high", "low"))).cache()

    it("should adjust the highs"){
      output.filter('dt === "2020-08-26").getDouble("high") shouldEqual 200D
      output.filter('dt === "2020-08-27").getDouble("high") shouldEqual 210D
      output.filter('dt === "2020-08-28").getDouble("high") shouldEqual 220D
      output.filter('dt === "2020-08-31").getDouble("high") shouldEqual 225D
      output.filter('dt === "2020-09-01").getDouble("high") shouldEqual 250D
      output.filter('dt === "2020-09-02").getDouble("high") shouldEqual 300D
      output.filter('dt === "2020-09-03").getDouble("high") shouldEqual 350D
      output.filter('dt === "2020-09-04").getDouble("high") shouldEqual 300D
      output.filter('dt === "2020-09-05").getDouble("high") shouldEqual 350D
      output.filter('dt === "2020-09-06").getDouble("high") shouldEqual 400D
      output.filter('dt === "2020-09-07").getDouble("high") shouldEqual 450D
    }
    it("should adjust the lows"){
      output.filter('dt === "2020-08-26").getDouble("low") shouldEqual 150D
      output.filter('dt === "2020-08-27").getDouble("low") shouldEqual 160D
      output.filter('dt === "2020-08-28").getDouble("low") shouldEqual 170D
      output.filter('dt === "2020-08-31").getDouble("low") shouldEqual 170D
      output.filter('dt === "2020-09-01").getDouble("low") shouldEqual 200D
      output.filter('dt === "2020-09-02").getDouble("low") shouldEqual 250D
      output.filter('dt === "2020-09-03").getDouble("low") shouldEqual 300D
      output.filter('dt === "2020-09-04").getDouble("low") shouldEqual 150D
      output.filter('dt === "2020-09-05").getDouble("low") shouldEqual 200D
      output.filter('dt === "2020-09-06").getDouble("low") shouldEqual 250D
      output.filter('dt === "2020-09-07").getDouble("low") shouldEqual 300D
    }
    it("should adjust the volumes"){
      output.filter('dt === "2020-08-26").getLong("volume") shouldEqual 100000L
      output.filter('dt === "2020-08-27").getLong("volume") shouldEqual 100000L
      output.filter('dt === "2020-08-28").getLong("volume") shouldEqual 100000L
      output.filter('dt === "2020-08-31").getLong("volume") shouldEqual 20000L
      output.filter('dt === "2020-09-01").getLong("volume") shouldEqual 20000L
      output.filter('dt === "2020-09-02").getLong("volume") shouldEqual 20000L
      output.filter('dt === "2020-09-03").getLong("volume") shouldEqual 20000L
      output.filter('dt === "2020-09-04").getLong("volume") shouldEqual 10000L
      output.filter('dt === "2020-09-05").getLong("volume") shouldEqual 10000L
      output.filter('dt === "2020-09-06").getLong("volume") shouldEqual 10000L
      output.filter('dt === "2020-09-07").getLong("volume") shouldEqual 10000L
    }



  }


}
