package com.etl.datalake

import com.core.SparkSessionTestWrapper
import com.core.TestUtils.{collectValuesOfColumnToList, prepareSchema}
import com.etl.datalake.TradingHolidaysJob.responseToDataFrame
import org.apache.spark.sql.DataFrame
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class TradingHolidaysJobSpec extends AnyFlatSpec with should.Matchers with SparkSessionTestWrapper {

  val response: String = {
    """
      |[{
      |   "date":"2021-01-01",
      |   "localName":"New Year's Day",
      |   "name":"New Year's Day",
      |   "countryCode":"US",
      |   "fixed":false,
      |   "global":true,
      |   "counties":null,
      |   "launchYear":null,
      |   "type":"Public"
      |},{
      |   "date":"2021-01-18",
      |   "localName":"Martin Luther King, Jr. Day",
      |   "name":"Martin Luther King, Jr. Day",
      |   "countryCode":"US",
      |   "fixed":false,
      |   "global":true,
      |   "counties":null,
      |   "launchYear":null,
      |   "type":"Public"
      |   },{
      |   "date":"2021-01-30",
      |   "localName":"Some other Day",
      |   "name":"Some other Day",
      |   "countryCode":"US",
      |   "fixed":false,
      |   "global":true,
      |   "counties":null,
      |   "launchYear":null,
      |   "type":"Public"
      |}]""".stripMargin
  }
  val output: DataFrame = responseToDataFrame(spark, response).cache()

  "responseToDataFrame" should "should output the expected schema" in {
    prepareSchema(output.schema) shouldEqual prepareSchema(tradingHolidaysSchema)
  }

  it should "have a count of 2" in {
    output.count should be(2)
  }

  it should "have the expected names" in {
    collectValuesOfColumnToList[String](output, "name").sortWith(_ < _) shouldEqual
      List("Martin Luther King, Jr. Day", "New Year's Day").sortWith(_ < _)
  }
}
