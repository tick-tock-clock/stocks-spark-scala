package com.etl.datalake

import com.core.SparkSessionTestWrapper
import org.apache.spark.sql.DataFrame
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should

class AggregatedStockDataJobSpec extends AnyFunSpec with should.Matchers with SparkSessionTestWrapper {

  import spark.implicits._

  val columns: Seq[String] = Seq("dt", "ticker", "open", "high", "low", "close", "year", "adjusted_year", "month", "week_of_year")
  val data: Seq[(String, String, Double, Double, Double, Double, Long, Long, Long, Long)] = Seq(
    //      FOO
    ("2020-12-28", "FOO", 1D, 1D, 1D, 1D, 2020L, 2020L, 12L, 53L),
    ("2020-12-29", "FOO", 2D, 3D, 0.5D, 4D, 2020L, 2020L, 12L, 53L),
    ("2020-12-30", "FOO", 3D, 5D, 3D, 5D, 2020L, 2020L, 12L, 53L),
    ("2020-12-31", "FOO", 2D, 3D, 2D, 3D, 2020L, 2020L, 12L, 53L),

    ("2021-01-04", "FOO", 10D, 11D, 10D, 10D, 2021L, 2021L, 1L, 1L),
    ("2021-01-05", "FOO", 11D, 13D, 9D, 11D, 2021L, 2021L, 1L, 1L),
    ("2021-01-06", "FOO", 12D, 14D, 5D, 11D, 2021L, 2021L, 1L, 1L),
    ("2021-01-07", "FOO", 13D, 15D, 9D, 12D, 2021L, 2021L, 1L, 1L),
    ("2021-01-08", "FOO", 14D, 14D, 10D, 13D, 2021L, 2021L, 1L, 1L),

    ("2021-01-11", "FOO", 100D, 110D, 100D, 100D, 2021L, 2021L, 1L, 2L),
    ("2021-01-12", "FOO", 110D, 130D, 90D, 110D, 2021L, 2021L, 1L, 2L),
    ("2021-01-13", "FOO", 120D, 140D, 50D, 120D, 2021L, 2021L, 1L, 2L),
    ("2021-01-14", "FOO", 130D, 150D, 90D, 120D, 2021L, 2021L, 1L, 2L),
    ("2021-01-15", "FOO", 140D, 140D, 100D, 130D, 2021L, 2021L, 1L, 2L),
    //      BAR
    ("2020-12-28", "BAR", 10D, 10D, 10D, 10D, 2020L, 2020L, 12L, 53L),
    ("2020-12-29", "BAR", 20D, 30D, 8D, 12D, 2020L, 2020L, 12L, 53L),
    ("2020-12-30", "BAR", 30D, 50D, 5D, 10D, 2020L, 2020L, 12L, 53L),
    ("2020-12-31", "BAR", 40D, 30D, 10D, 13D, 2020L, 2020L, 12L, 53L),

    ("2021-01-04", "BAR", 100D, 110D, 100D, 10D, 2021L, 2021L, 1L, 1L),
    ("2021-01-05", "BAR", 101D, 130D, 80D, 110D, 2021L, 2021L, 1L, 1L),
    ("2021-01-06", "BAR", 102D, 140D, 50D, 120D, 2021L, 2021L, 1L, 1L),
    ("2021-01-07", "BAR", 103D, 150D, 60D, 110D, 2021L, 2021L, 1L, 1L),
    ("2021-01-08", "BAR", 104D, 140D, 100D, 130D, 2021L, 2021L, 1L, 1L),

    ("2021-01-11", "BAR", 1000D, 1100D, 1000D, 1000D, 2021L, 2021L, 1L, 2L),
    ("2021-01-12", "BAR", 1100D, 1300D, 800D, 1100D, 2021L, 2021L, 1L, 2L),
    ("2021-01-13", "BAR", 1200D, 1400D, 500D, 1200D, 2021L, 2021L, 1L, 2L),
    ("2021-01-14", "BAR", 1300D, 1500D, 600D, 1200D, 2021L, 2021L, 1L, 2L),
    ("2021-01-15", "BAR", 1400D, 1400D, 1000D, 1300D, 2021L, 2021L, 1L, 2L)
  )
  val input: DataFrame = data.toDF(columns: _*)

  val weeklyFilters = Seq(
    $"dt" === "2020-12-28" && $"ticker" === "FOO",
    $"dt" === "2021-01-04" && $"ticker" === "FOO",
    $"dt" === "2021-01-11" && $"ticker" === "FOO",
    $"dt" === "2020-12-28" && $"ticker" === "BAR",
    $"dt" === "2021-01-04" && $"ticker" === "BAR",
    $"dt" === "2021-01-11" && $"ticker" === "BAR",
  )
  val monthlyFilters = Seq(
    $"dt" === "2020-12-28" && $"ticker" === "FOO",
    $"dt" === "2021-01-04" && $"ticker" === "FOO",
    $"dt" === "2020-12-28" && $"ticker" === "BAR",
    $"dt" === "2021-01-04" && $"ticker" === "BAR",
  )

  describe("addDateSpecifics") {

    val output: DataFrame = AggregatedStockDataJob
      .addDateSpecifics(input.select("dt", "ticker")).cache()

    it("extends DataFrame by year ") {
      (Seq(2020L, 2021L, 2021L, 2020L, 2021L, 2021L), weeklyFilters).zipped.foreach((year, filter) =>
        output.filter(filter).getLong("year") shouldEqual year
      )
    }
    it("extends DataFrame by month") {
      (Seq(12L, 1L, 1L, 12L, 1L, 1L), weeklyFilters).zipped.foreach((month, filter) =>
        output.filter(filter).getLong("month") shouldEqual month
      )
    }
    it("extends DataFrame by week of year") {
      (Seq(53L, 1L, 2L, 53L, 1L, 2L), weeklyFilters)
        .zipped.foreach((weekOfYear, filter) =>
        output.filter(filter).getLong("week_of_year") shouldEqual weekOfYear
      )
    }
  }

  describe("aggregateOhlcToPeriod") {

    describe("weekly") {
      val output: DataFrame = AggregatedStockDataJob
        .aggregateOhlcToPeriod(
          input.select(
            "dt", "ticker", "open", "high", "low", "close", "year", "adjusted_year", "month", "week_of_year"),
          "weekly")
        .cache()

      it("aggregates on weekly level") {
        output.count shouldEqual 6
      }
      it("produces the expected opens") {
        (Seq[Double](1D, 10D, 100D, 10D, 100D, 1000D), weeklyFilters).zipped.foreach(
          (open, filter) => output.filter(filter).getDouble("open") shouldEqual open
        )
      }
      it("produces the expected highs") {
        (Seq[Double](5D, 15D, 150D, 50D, 150D, 1500D), weeklyFilters).zipped.foreach(
          (high, filter) => output.filter(filter).getDouble("high") shouldEqual high
        )
      }
      it("produces the expected low") {
        (Seq[Double](0.5D, 5D, 50D, 5D, 50D, 500D), weeklyFilters).zipped.foreach(
          (low, filter) => output.filter(filter).getDouble("low") shouldEqual low
        )
      }
      it("produces the expected closes") {
        (Seq[Double](3D, 13D, 130D, 13D, 130D, 1300D), weeklyFilters).zipped.foreach(
          (close, filter) => output.filter(filter).getDouble("close") shouldEqual close
        )
      }

    }
    describe("monthly") {
      val output: DataFrame = AggregatedStockDataJob
        .aggregateOhlcToPeriod(
          input.select(
            "dt", "ticker", "open", "high", "low", "close", "year", "month", "week_of_year"),
          "monthly")
        .cache()

      it("aggregates on monthly level") {
        output.count shouldEqual 4
      }
      it("produces the expected opens") {
        (Seq[Double](1D, 10D, 10D, 100D), monthlyFilters).zipped.foreach(
          (open, filter) => output.filter(filter).getDouble("open") shouldEqual open
        )
      }
      it("produces the expected highs") {
        (Seq[Double](5D, 150D, 50D, 1500D), monthlyFilters).zipped.foreach(
          (high, filter) => output.filter(filter).getDouble("high") shouldEqual high
        )
      }
      it("produces the expected low") {
        (Seq[Double](0.5D, 5D, 5D, 50D), monthlyFilters).zipped.foreach(
          (low, filter) => output.filter(filter).getDouble("low") shouldEqual low
        )
      }
      it("produces the expected closes") {
        (Seq[Double](3D, 130D, 13D, 1300D), monthlyFilters).zipped.foreach(
          (close, filter) => output.filter(filter).getDouble("close") shouldEqual close
        )
      }

    }


  }

  describe("process") {
    describe("weekly") {
      val output: DataFrame = AggregatedStockDataJob
        .process(input.select("dt", "ticker", "open", "high", "low", "close"), "weekly")
        .cache()

      it("aggregates on weekly level") {
        output.count shouldEqual 6
      }
      it("produces the expected opens") {
        (Seq[Double](1D, 10D, 100D, 10D, 100D, 1000D), weeklyFilters).zipped.foreach(
          (open, filter) => output.filter(filter).getDouble("open") shouldEqual open
        )
      }
      it("produces the expected highs") {
        (Seq[Double](5D, 15D, 150D, 50D, 150D, 1500D), weeklyFilters).zipped.foreach(
          (high, filter) => output.filter(filter).getDouble("high") shouldEqual high
        )
      }
      it("produces the expected low") {
        (Seq[Double](0.5D, 5D, 50D, 5D, 50D, 500D), weeklyFilters).zipped.foreach(
          (low, filter) => output.filter(filter).getDouble("low") shouldEqual low
        )
      }
      it("produces the expected closes") {
        (Seq[Double](3D, 13D, 130D, 13D, 130D, 1300D), weeklyFilters).zipped.foreach(
          (close, filter) => output.filter(filter).getDouble("close") shouldEqual close
        )
      }
    }
    describe("monthly") {
      val output: DataFrame = AggregatedStockDataJob
        .process(input.select("dt", "ticker", "open", "high", "low", "close"), "monthly")
        .cache()

      it("aggregates on monthly level") {
        output.count shouldEqual 4
      }
      it("produces the expected opens") {
        (Seq[Double](1D, 10D, 10D, 100D), monthlyFilters).zipped.foreach(
          (open, filter) => output.filter(filter).getDouble("open") shouldEqual open
        )
      }
      it("produces the expected highs") {
        (Seq[Double](5D, 150D, 50D, 1500D), monthlyFilters).zipped.foreach(
          (high, filter) => output.filter(filter).getDouble("high") shouldEqual high
        )
      }
      it("produces the expected low") {
        (Seq[Double](0.5D, 5D, 5D, 50D), monthlyFilters).zipped.foreach(
          (low, filter) => output.filter(filter).getDouble("low") shouldEqual low
        )
      }
      it("produces the expected closes") {
        (Seq[Double](3D, 130D, 13D, 1300D), monthlyFilters).zipped.foreach(
          (close, filter) => output.filter(filter).getDouble("close") shouldEqual close
        )
      }

    }

  }


}
