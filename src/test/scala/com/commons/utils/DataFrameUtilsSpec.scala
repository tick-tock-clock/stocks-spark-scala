package com.commons.utils

import com.commons.utils.DataFrameUtils._
import com.core.SparkSessionTestWrapper
import com.core.TestUtils.prepareSchema
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.{DateType, LongType, StringType, StructType}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

import java.time.Instant

class DataFrameUtilsSpec extends AnyFlatSpec with should.Matchers with SparkSessionTestWrapper {

  import spark.implicits._

  "selectWithSchema" should "Select from dataframe with specified schema" in {
    val df: DataFrame = Seq((1, "foo", "bar")).toDF("a", "b", "c")

    val schema: StructType = new StructType()
      .add("a", LongType, nullable = false)
      .add("b", StringType, nullable = false)

    val output: DataFrame = df.transform(selectWithSchema(_, schema))

    prepareSchema(output.schema) shouldEqual prepareSchema(schema)
    output.columns.sorted shouldEqual List("a", "b").sorted
    output.getLong("a") shouldEqual 1
    output.getString("b") shouldEqual "foo"

  }
  "snakeCaseColumns" should "Add underscore to columns with spaces" in {
    val df: DataFrame = Seq((1, 2)).toDF("foo", "bar coo")
    val output: DataFrame = df.transform(snakeCaseColumns)

    output.columns.sorted shouldEqual List("foo", "bar_coo").sorted
  }

  "getDistinctListFromColumn" should "Create a List of distinct values that exist in a column" in {
    val input: DataFrame = Seq((1L, "Foo"), (2L, "Bar")).toDF("long", "string")

    getDistinctListFromColumn[Long](input, "long") shouldEqual List(1L, 2L)
    getDistinctListFromColumn[String](input, "string") shouldEqual List("Foo", "Bar")
  }

  "dataFrameDateColumnToInstants" should "Create list of Instant for each Date in date column" in {
    val input: DataFrame = Seq(("2021-01-01", "Foo"), ("2021-01-02", "Bar")).toDF("dt", "string")

    dataFrameDateColumnToInstants(input, "dt") shouldEqual List(
      Instant.parse("2021-01-01T00:00:00Z"), Instant.parse("2021-01-02T00:00:00Z")
    )
  }

  "emptyDataFrame" should "Create a empty dataframe from schema" in {
    val schema = new StructType().add("foo", StringType).add("bar", DateType)
    val output = emptyDataFrame(spark, schema).cache()

    output.columns.toList shouldEqual List("foo", "bar")
    output.count() should be(0)
    prepareSchema(output.schema) shouldEqual prepareSchema(schema)

  }


}
