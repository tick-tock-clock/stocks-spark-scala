package com.commons.utils

import com.core.SparkSessionTestWrapper
import org.apache.hadoop.fs.Path
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class JobUtilsSpec extends AnyFlatSpec with should.Matchers with SparkSessionTestWrapper {



  "generateAllPartitionPaths" should "Generate an empty array without partitions" in {
    assert(JobUtils.generatePartitionPaths(
      new Path("s3://the-foo-bucket"), List[PartitionByDay]()
    ).isEmpty)
  }
  it should "Generate the expected paths for each partition" in {
    JobUtils.generatePartitionPaths(
      new Path("s3://the-foo-bucket/some_table"),
      List[PartitionByDay](PartitionByDay("2020-01-01"), PartitionByDay("2020-01-02"), PartitionByDay("2020-01-03")))
      .map(_.toString) shouldEqual Array(
      "s3://the-foo-bucket/some_table/dt=2020-01-01",
      "s3://the-foo-bucket/some_table/dt=2020-01-02",
      "s3://the-foo-bucket/some_table/dt=2020-01-03"
    )
  }

  "appendPreviousWindowOfPartitions" should
    "Generate a List of PartitionByDay from window and prepends it to existing List" in {

    val partitions: List[PartitionByDay] = List(
      PartitionByDay("2021-01-01"), PartitionByDay("2021-01-02"))

    JobUtils.appendPreviousWindowOfPartitions(partitions, 2).sorted shouldEqual List(
      PartitionByDay("2020-12-30"), PartitionByDay("2020-12-31"),
      PartitionByDay("2021-01-01"), PartitionByDay("2021-01-02")
    ).sorted
  }
  "appendFollowingWindowOfPartitions" should
    "Generate a List of PartitionByDay from window and prepends it to existing List" in {

    val partitions: List[PartitionByDay] = List(
      PartitionByDay("2021-01-01"), PartitionByDay("2021-01-02"))

    JobUtils.appendFollowingWindowOfPartitions(partitions, 2).sorted shouldEqual List(
      PartitionByDay("2021-01-01"), PartitionByDay("2021-01-02"),
      PartitionByDay("2021-01-03"), PartitionByDay("2021-01-04")
    ).sorted
  }

  "generatePartitionsByTicker" should "Build a List of PartitionByTicker from tickers" in {
    val tickers: List[String] = List("FOO", "BAR")
    val partitions: List[PartitionByDay] = List(
      PartitionByDay("2021-01-01"), PartitionByDay("2021-01-02"))

    JobUtils.generatePartitionsByTicker(tickers, partitions).sorted shouldEqual List(
      PartitionByTicker("2021-01-01", "FOO"), PartitionByTicker("2021-01-01", "BAR"),
      PartitionByTicker("2021-01-02", "FOO"), PartitionByTicker("2021-01-02", "BAR")
    ).sorted
  }

}
