package com.commons.utils

import com.core.SparkSessionTestWrapper
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class PartitionByDaySpec extends AnyFlatSpec with should.Matchers with SparkSessionTestWrapper {

  val partition: PartitionByDay = PartitionByDay("2020-01-15")

  "addDays" should "add n days to partition" in {
    partition.addDays(10).dt shouldEqual "2020-01-25"
  }
  "subtractDays" should "subtract n days to partition" in {
    partition.subtractDays(10).dt shouldEqual "2020-01-05"
  }

  "previousDays" should
    "build a list of partitions from window applied to partition and exclude applied partition" in {
    partition.previousDays(3) shouldEqual List(
      PartitionByDay("2020-01-14"),
      PartitionByDay("2020-01-13"),
      PartitionByDay("2020-01-12")
    )
  }
  it should "throw an error when setting window less than 1" in {
    assertThrows[IllegalArgumentException](partition.previousDays(-1))
    assertThrows[IllegalArgumentException](partition.previousDays(0))
  }
  "followingDays" should
    "build a list of partitions from window applied to partition and exclude applied partition" in {
    partition.followingDays(3) shouldEqual List(
      PartitionByDay("2020-01-16"),
      PartitionByDay("2020-01-17"),
      PartitionByDay("2020-01-18")
    )
  }
  it should "throw an error when setting window less than 1" in {
    assertThrows[IllegalArgumentException](partition.previousDays(-1))
    assertThrows[IllegalArgumentException](partition.previousDays(0))
  }

  "until" should "Generate all partitions between partitions except last partition" in {
    PartitionByDay("2020-01-01").until(PartitionByDay("2020-01-05")).toSet shouldEqual Set(
      PartitionByDay("2020-01-01"),
      PartitionByDay("2020-01-02"),
      PartitionByDay("2020-01-03"),
      PartitionByDay("2020-01-04")
    )
  }

  "to" should "Generate all partitions between partitions including last partition" in {
    PartitionByDay("2020-01-01").to(PartitionByDay("2020-01-05")).toSet shouldEqual Set(
      PartitionByDay("2020-01-01"),
      PartitionByDay("2020-01-02"),
      PartitionByDay("2020-01-03"),
      PartitionByDay("2020-01-04"),
      PartitionByDay("2020-01-05")
    )
  }
  it should "Return a list of end partition if start equals end" in {
    PartitionByDay("2020-01-01").to(PartitionByDay("2020-01-01")).toSet shouldEqual
      Set(PartitionByDay("2020-01-01"))
  }

}
