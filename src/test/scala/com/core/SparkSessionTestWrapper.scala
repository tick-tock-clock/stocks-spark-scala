package com.core

import org.apache.spark.sql.{DataFrame, SparkSession}


trait SparkSessionTestWrapper {

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .config("spark.sql.shuffle.partitions", 2)
      .master("local")
      .appName("Test")
      .getOrCreate()
  }

  implicit def wrapDataFrame(df: DataFrame): DataFrameTester = new DataFrameTester(df)

}
