package com.core

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.col

class DataFrameTester(dataFrame: DataFrame) {
  def getString(colName: String): String = {
    enforceSingleValue()
    this.dataFrame.select(col(colName).cast("string")).collect.map(_.getString(0)).head
  }

  def enforceSingleValue(): Unit = {
    val rowCount = this.dataFrame.count
    if (rowCount != 1) {
      throw new IllegalArgumentException(s"Expected a dataframe with only one row. But got ${rowCount}")
    }
  }

  def getLong(colName: String): java.lang.Long = {
    enforceSingleValue()
    this.dataFrame.select(col(colName)).collect.map(_.get(0).asInstanceOf[java.lang.Long]).head
  }

  def getDouble(colName: String): Double = {
    enforceSingleValue()
    this.dataFrame.select(col(colName)).collect.map(_.getDouble(0)).head
  }

  def isNull(colName: String): Boolean = {
    enforceSingleValue()
    this.dataFrame.select(col(colName)).collect.map(_.isNullAt(0)).head
  }

  def getBoolean(colName: String): Boolean = {
    enforceSingleValue()
    this.dataFrame.select(col(colName)).collect.map(_.getBoolean(0)).head
  }
}
