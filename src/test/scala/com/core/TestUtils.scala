package com.core

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.{StructField, StructType}

import scala.reflect.ClassTag

object TestUtils {
  /**
   * Extracts all fields from a schema without looking at the nullable-flag.
   * This makes it easier to compare the schema in tests
   *
   * @param schema
   * @return Sequence of all fields of the schema (name + data type)
   */
  def prepareSchema(schema: StructType): Seq[StructField] = {
    schema.map(field => StructField(field.name, field.dataType))
  }

  def collectValuesOfColumnToList[T:ClassTag](df:DataFrame, column: String):List[T]={
    df.rdd.map(_.getAs[T](column)).collect().toList
  }

}
