package com.commons.traits

trait HivePartition {
  def toHivePartitionPath: String = keys.zip(values).map(kv => kv._1 + "=" + kv._2).mkString("/")

  def values: Array[String]

  def keys: Array[String]
}
