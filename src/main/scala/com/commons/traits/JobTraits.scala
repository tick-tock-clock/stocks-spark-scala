package com.commons.traits

import org.apache.spark.sql.SparkSession
import org.slf4j.{Logger, LoggerFactory}

import java.time.Instant

trait JobTraits {
  val logger: Logger = LoggerFactory.getLogger(getClass.getSimpleName)

  def verifyArgs(args: Array[String], argNames: String*): Unit = {
    if (args.length != argNames.length) {
      throw new IllegalArgumentException(
        s"Usage ${getClass.getSimpleName} " +
          argNames.map(argName => s"[$argName]").mkString(" "))
    }
  }

  def logArgs(args: (String, Any)*): Unit = args.foreach(arg =>
    logger.warn(s"${arg._1} --> ${arg._2.toString}"))

  def newSparkSession(appNameExtension: String = ""): SparkSession = {
    SparkSession.builder()
      .appName(getClass.getSimpleName + appNameExtension)
      .enableHiveSupport()
      .getOrCreate()
  }

  def setupLocalLogger(): Unit = {
    import org.apache.log4j.PropertyConfigurator
    val log4jConfPath = "/home/tk/code/signals/src/main/resources/log4j.properties"
    PropertyConfigurator.configure(log4jConfPath)
  }

  def timeJob[T](job: => T): T = {
    val start: Instant = takeCurrentTime
    val res: T = job
    logTimeItTook(start, takeCurrentTime)
    res
  }

  def takeCurrentTime: Instant = Instant.now()

  def logTimeItTook(start: Instant, end: Instant): Unit = logger
    .warn(s"Job took ${instantDifferenceInMinutes(start, end)} minutes")

  def instantDifferenceInMinutes(start: Instant, end: Instant): Double = {
    val seconds: Double = end.minusSeconds(start.getEpochSecond)
      .getEpochSecond.asInstanceOf[Double]
    BigDecimal(seconds / 60).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
  }

}
