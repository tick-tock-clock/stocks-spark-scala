package com.commons.utils

import com.commons.traits.JobTraits

import java.io.InputStream
import java.lang.Thread.sleep
import java.net.{HttpURLConnection, SocketTimeoutException, URL}
import scala.io.Source.fromInputStream

object HTTPHandler extends JobTraits {

  private val ALPHA_VANTAGE_API_LIMIT_WARNING: String = "5 calls per minute and 500 calls per day"

  /**
   * @param url            The full URL to connect to.
   * @param connectTimeout Sets a specified timeout value, in milliseconds,
   *                       to be used when opening a communications link to the resource referenced
   *                       by this URLConnection. If the timeout expires before the connection can
   *                       be established, a java.net.SocketTimeoutException
   *                       is raised. A timeout of zero is interpreted as an infinite timeout.
   *                       Defaults to 5000 ms.
   * @param readTimeout    If the timeout expires before there is data available
   *                       for read, a java.net.SocketTimeoutException is raised. A timeout of zero
   *                       is interpreted as an infinite timeout. Defaults to 5000 ms.
   */
  @throws(classOf[java.io.IOException])
  @throws(classOf[java.net.SocketTimeoutException])
  def get(url: String,
          connectTimeout: Int = 5000,
          readTimeout: Int = 5000): String = {

    val connection: HttpURLConnection = new URL(url).openConnection.asInstanceOf[HttpURLConnection]
    connection.setConnectTimeout(connectTimeout)
    connection.setReadTimeout(readTimeout)
    connection.setRequestMethod("GET")

    try {
      connection.getResponseCode match {
        case 429 =>
          logger.warn("API returned 429. Sleeping for 5 seconds and try again.")
          sleep(5000)
          get(url, connectTimeout, readTimeout)
        case _ =>

          val inputStream: InputStream = connection.getInputStream
          fromInputStream(inputStream).mkString match {
            case alphaVantageContent: String if alphaVantageContent
              .contains(ALPHA_VANTAGE_API_LIMIT_WARNING) =>
              logger.warn(s"Sleeping for 20 seconds | $ALPHA_VANTAGE_API_LIMIT_WARNING")
              sleep(20000)
              inputStream.close()
              get(url, connectTimeout, readTimeout)
            case content: String => inputStream.close(); content
          }
      }
    }
    catch {
      case _: SocketTimeoutException =>
        logger.warn(s"Sleeping for 5 seconds | SocketTimeoutException")
        sleep(5000)
        get(url, connectTimeout, readTimeout)
      case error: Throwable => throw new RuntimeException(error)
    }
  }
}
