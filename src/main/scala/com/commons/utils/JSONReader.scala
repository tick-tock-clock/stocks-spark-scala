package com.commons.utils

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper

object JSONReader {
  val mapper = new ObjectMapper() with ScalaObjectMapper
  mapper.registerModule(DefaultScalaModule)
  mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  def toJson(value: Any): String = mapper.writeValueAsString(value)

  def toMap[V](json: String)(implicit m: Manifest[V]): Map[String, Any] = fromJson[Map[String, Any]](json)

  private def fromJson[T](json: String)(implicit m: Manifest[T]): T = mapper.readValue[T](json)

  def toCaseClass[T](json: String)(implicit m: Manifest[T]): T = from[T](fromJson[Map[String, Any]](json))

  private def from[T](map: Map[String, Any])(implicit m: Manifest[T]): T = mapper.convertValue(map)

}