package com.commons.utils

import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

import java.sql.Date
import java.time.Instant
import scala.reflect.ClassTag

object DataFrameUtils {
  /**
   * Select statement of DataFrame from desired Schema.
   *
   * @param df
   * @param schema
   * @return
   */
  def selectWithSchema(df: DataFrame, schema: StructType): DataFrame = {
    df.select(schema.fields.map(field => col(field.name).cast(field.dataType)): _*)
  }

  /**
   * Snake cases space separated column names in DataFrame.
   *
   * @param input
   * @return
   */
  def snakeCaseColumns(input: DataFrame): DataFrame = {
    input.columns.foldLeft(input) { (memoDF, colName) =>
      memoDF.withColumnRenamed(colName, colName.replace(" ", "_"))
    }
  }

  /**
   * Create list of distinct values that exist in a column.
   *
   * @param df      input DataFrame
   * @param colName name of the date column
   * @tparam T type of column
   * @return list of elements in the column
   */
  def getDistinctListFromColumn[T: ClassTag](df: DataFrame, colName: String): List[T] = {
    df.rdd.map(_.getAs[T](colName)).distinct.collect().toList
  }

  /**
   * Creates empty DataFrame from schema.
   *
   * @param spark
   * @param schema
   * @return
   */
  def emptyDataFrame(spark: SparkSession, schema: StructType): DataFrame = {
    spark.sqlContext.createDataFrame(spark.sparkContext.emptyRDD[Row], schema)
  }


  /**
   * Returns a List of Instant for each Date in date column.
   *
   * @param input
   * @param dateColumn
   * @return
   */
  def dataFrameDateColumnToInstants(input: DataFrame, dateColumn: String): List[Instant] = {
    input.rdd.map(row =>
      Instant.parse(row.getAs[Date](dateColumn) + "T00:00:00Z")
    ).distinct.collect.toList
  }

}
