package com.commons.utils

import com.commons.traits.HivePartition

import java.time.{Instant, ZoneId}
import java.time.format.DateTimeFormatter

case class PartitionByTicker(dt: String, ticker: String) extends HivePartition with Ordered[PartitionByTicker] {
  override def values: Array[String] = Array[String](dt, ticker)

  override def keys: Array[String] = Array[String]("dt", "ticker")

  override def compare(that: PartitionByTicker): Int = this.toInstant.compareTo(that.toInstant)

  def toInstant: Instant = Instant.parse(dt + "T00:00:00Z")
}

object PartitionByTicker {
  val dtFormat: DateTimeFormatter = DateTimeFormatter
    .ofPattern("yyyy-MM-dd").withZone(ZoneId.of("UTC"))
}
