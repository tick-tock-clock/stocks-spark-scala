package com.commons.utils

import com.commons.utils.JobUtils.{exists, generatePartitionPaths}
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.functions.{col, max}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.storage.StorageLevel
import org.slf4j.{Logger, LoggerFactory}

class CustomSparkSession(spark: SparkSession) {

  val LOCAL_PATH = "/tmp"

  val ROWS_PER_PARTITION: Long = 10000

  /**
   *
   * @param layer
   * @param table
   * @param partitions
   * @return
   */
  def readPartitionsByDayFromDatabase(layer: String,
                                      table: String,
                                      partitions: List[PartitionByDay]): Option[DataFrame] = {

    val config: CustomConfig = CustomConfig(layer, table)
    val basePath: Path = new Path(s"$LOCAL_PATH/${config.getOutputPath}")
    val paths: List[Path] = generatePartitionPaths(basePath, partitions)
    readValidPartitionPaths(basePath, paths)
  }

  /**
   *
   * @param layer
   * @param table
   * @param partitions
   * @return
   */
  def readPartitionsByTickerFromDatabase(layer: String,
                                         table: String,
                                         partitions: List[PartitionByTicker]): Option[DataFrame] = {

    val config: CustomConfig = CustomConfig(layer, table)
    val basePath: Path = new Path(s"$LOCAL_PATH/${config.getOutputPath}")
    val paths: List[Path] = generatePartitionPaths(basePath, partitions)
    readValidPartitionPaths(basePath, paths)
  }

  /**
   *
   * @param basePath
   * @param partitionPaths
   * @return
   */
  def readValidPartitionPaths(basePath: Path, partitionPaths: List[Path]): Option[DataFrame] = {
    val validPaths: List[Path] = partitionPaths.filter(exists(spark, _))
    if (validPaths.isEmpty) None else Some(readParquet(basePath, validPaths))
  }

  /**
   *
   * @param basePath
   * @param partitionPaths
   * @return
   */
  def readParquet(basePath: Path, partitionPaths: List[Path]): DataFrame = {
    spark
      .read
      .option("mergeSchema", true)
      .option("basePath", basePath.toString)
      .format("parquet")
      .load(partitionPaths.map(_.toString): _*)
  }

  /**
   *
   * @param df
   * @param config
   * @param partitionBy
   */
  def writeDataFrameToDatabase(df: DataFrame,
                               config: CustomConfig,
                               partitionBy: String*): Unit = {

    val cachedDf: DataFrame = df.persist(StorageLevel.DISK_ONLY)

    val dfCount: Long = cachedDf
      .groupBy(partitionBy.head, partitionBy.tail: _*)
      .count
      .agg(max(col("count"))).head().getLong(0)

    dfCount match {
      case 0 => CustomSparkSession.logger.warn("The dataFrame is empty, skipping writing...")
      case count =>
        val repartition: Int = repartitionEstimator(count)
        CustomSparkSession.logger.warn(s"Writing ${cachedDf.count} Rows to destination")
        spark.conf.set("spark.sql.sources.partitionOverwriteMode", "dynamic")
        cachedDf
          .repartition(repartition)
          .write
          .option("compression", "gzip")
          .mode(SaveMode.Overwrite)
          .partitionBy(partitionBy: _*)
          .parquet(s"$LOCAL_PATH/${config.getOutputPath}")
        CustomSparkSession.logger.warn(s"Writing was Successful")
    }
  }

  /**
   *
   * @param count
   * @return
   */
  def repartitionEstimator(count: Long): Int = Math.max(count / ROWS_PER_PARTITION, 1).toInt
}

object CustomSparkSession {
  val logger: Logger = LoggerFactory.getLogger(getClass.getSimpleName)

  object implicits {
    implicit def extendSparkSession(sparkSession: SparkSession): CustomSparkSession = {
      new CustomSparkSession(sparkSession)
    }
  }

}