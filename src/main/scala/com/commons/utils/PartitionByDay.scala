package com.commons.utils

import com.commons.traits.HivePartition

import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.time.{Instant, ZoneId}

case class PartitionByDay(dt: String) extends HivePartition with Ordered[PartitionByDay] {

  override def values: Array[String] = Array[String](dt)

  override def keys: Array[String] = Array[String]("dt")

  override def compare(that: PartitionByDay): Int = this.toInstant.compareTo(that.toInstant)

  def toInstant: Instant = Instant.parse(dt + "T00:00:00Z")

  def previousDays(window: Int): List[PartitionByDay] = {
    require(window > 0, s"nbrDays should be >= 0, input: $window")
    (1 to window).map(index => subtractDays(index)).toList
  }

  def subtractDays(numberOfDays: Int): PartitionByDay = addDays(-numberOfDays)

  def followingDays(window: Int): List[PartitionByDay] = {
    require(window > 0, s"nbrDays should be >= 0, input: $window")
    (1 to window).map(index => addDays(index)).toList
  }

  def addDays(numberOfDays: Int): PartitionByDay = {
    val instant: Instant = toInstant.plus(numberOfDays, ChronoUnit.DAYS)
    PartitionByDay(PartitionByDay.dtFormat.format(instant))
  }

  def to(other: PartitionByDay): List[PartitionByDay] = {
    if (other < this) List() else until(other) :+ other
  }

  def until(other: PartitionByDay): List[PartitionByDay] = {
    var partitions: List[PartitionByDay] = List()
    if (other > this) {
      var currentPartition: PartitionByDay = this
      while (currentPartition < other) {
        partitions = partitions :+ currentPartition
        currentPartition = currentPartition.nextDay
      }
    }
    partitions
  }

  def nextDay: PartitionByDay = addDays(1)

  def toPartitionByTicker(ticker: String): PartitionByTicker = PartitionByTicker(dt, ticker)
}

object PartitionByDay {
  val dtFormat: DateTimeFormatter = DateTimeFormatter
    .ofPattern("yyyy-MM-dd").withZone(ZoneId.of("UTC"))
}