package com.commons.utils

import com.commons.traits.HivePartition
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.SparkSession

object JobUtils {

  /**
   * Given a List of tickers and desired List of PartitionByDay,
   * build List of PartitionByTicker from that.
   *
   * @param tickers
   * @param partitions
   * @return
   */
  def generatePartitionsByTicker(tickers: List[String],
                                 partitions: List[PartitionByDay]): List[PartitionByTicker] =
    tickers.flatMap(ticker => partitions
      .map(partition => partition.toPartitionByTicker(ticker)))


  /**
   * It takes the min partition of an existing list and generates a List of previous PartitionByDay
   * for the amount of days specified by the window and appends it to existing List.
   *
   * @param partitions
   * @param window
   * @return
   */
  def appendPreviousWindowOfPartitions(partitions: List[PartitionByDay],
                                       window: Int): List[PartitionByDay] = {
    partitions.min.previousDays(window) ++ partitions
  }

  /**
   * It takes the max partition of an existing list and generates a List of following PartitionByDay
   * for the amount of days specified by the window and appends it to existing List.
   *
   * @param partitions
   * @param window
   * @return
   */
  def appendFollowingWindowOfPartitions(partitions: List[PartitionByDay],
                                        window: Int): List[PartitionByDay] = {
    partitions.max.followingDays(window) ++ partitions
  }


  /**
   * Creates Hive Partition Paths from List of HivePartitions.
   *
   * @param basePath
   * @param partitions
   * @return
   */
  def generatePartitionPaths(basePath: Path, partitions: List[HivePartition]): List[Path] = {
    partitions.map(partition => new Path(s"$basePath/${partition.toHivePartitionPath}"))
  }

  /**
   * Checks that a file exists.
   */
  def exists(sparkSession: SparkSession, path: Path): Boolean = {
    path.getFileSystem(sparkSession.sparkContext.hadoopConfiguration).exists(path)
  }

  def getStartAndEndDateOfYear(year: Long): List[String] = {
    year match {
      case 1999 => List("1999-01-04", "1999-12-31")
      case 2000 => List("2000-01-03", "2000-12-29")
      case 2001 => List("2001-01-01", "2001-12-28")
      case 2002 => List("2001-12-31", "2002-12-27")
      case 2003 => List("2002-12-30", "2003-12-26")
      case 2004 => List("2003-12-29", "2004-12-31")
      case 2005 => List("2005-01-03", "2005-12-30")
      case 2006 => List("2006-01-02", "2006-12-29")
      case 2007 => List("2007-01-01", "2007-12-28")
      case 2008 => List("2007-12-31", "2008-12-26")
      case 2009 => List("2008-12-29", "2010-01-01")
      case 2010 => List("2010-01-04", "2010-12-31")
      case 2011 => List("2011-01-03", "2011-12-30")
      case 2012 => List("2012-01-02", "2012-12-28")
      case 2013 => List("2012-12-31", "2013-12-27")
      case 2014 => List("2013-12-30", "2014-12-26")
      case 2015 => List("2014-12-29", "2016-01-01")
      case 2016 => List("2016-01-04", "2016-12-30")
      case 2017 => List("2017-01-02", "2017-12-29")
      case 2018 => List("2018-01-01", "2018-12-28")
      case 2019 => List("2018-12-31", "2019-12-27")
      case 2020 => List("2019-12-30", "2021-01-01")
      case 2021 => List("2021-01-04", "2021-12-31")
      case 2022 => List("2022-01-03", "2022-12-30")
      case 2023 => List("2023-01-02", "2023-12-29")
    }

  }


}
