package com.commons.utils

import com.typesafe.config.{Config, ConfigFactory}

case class CustomConfig(layer: String, outputTable: String) {

  def getSecretsConfig: Config = ConfigFactory.load("secrets.conf")

  def getOutputPath: String = s"$getDatabasePath/$outputTable"

  def getDatabasePath: String = getDatabaseConfig.getString(s"$layer.path")

  def getDatabaseConfig: Config = ConfigFactory.load("database.conf")

}
