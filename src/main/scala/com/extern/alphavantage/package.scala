package com.extern

import org.apache.spark.sql.types._

package object alphavantage {
  val BASE_URL = "https://www.alphavantage.co/"
  val baseResponseSchema: StructType = new StructType()
    .add("timestamp", TimestampType, nullable = false)
    .add("open", DoubleType, nullable = false)
    .add("high", DoubleType, nullable = false)
    .add("low", DoubleType, nullable = false)
    .add("close", DoubleType, nullable = false)

  val dailyResponseSchema: StructType = baseResponseSchema
    .add("adjusted_close", DoubleType, nullable = false)
    .add("volume", LongType, nullable = false)
    .add("dividend_amount", DoubleType, nullable = false)
    .add("split_coefficient", DoubleType, nullable = false)

  val weeklyResponseSchema: StructType = baseResponseSchema
    .add("adjusted close", DoubleType, nullable = false)
    .add("volume", LongType, nullable = false)
    .add("dividend amount", DoubleType, nullable = false)

  val monthlyResponseSchema: StructType = weeklyResponseSchema

  val baseOutputSchema: StructType = new StructType()
    .add("dt", DateType, nullable = false)
    .add("ticker", StringType, nullable = false)
    .add("open", DoubleType, nullable = false)
    .add("high", DoubleType, nullable = false)
    .add("low", DoubleType, nullable = false)
    .add("close", DoubleType, nullable = false)
    .add("volume", LongType, nullable = false)
    .add("adjusted_close", DoubleType, nullable = false)
    .add("dividend_amount", DoubleType, nullable = false)

  val dailyOutputSchema: StructType = baseOutputSchema
    .add("split_coefficient", DoubleType, nullable = false)

  val weeklyOutputSchema: StructType = baseOutputSchema

  val monthlyOutputSchema: StructType = baseOutputSchema

  def pickAlphaVantageSchema(schemaType: String, period: String): StructType = {
    schemaType match {
      case "response" => period match {
        case "daily" => dailyResponseSchema
        case "weekly" => weeklyResponseSchema
        case "monthly" => monthlyResponseSchema
      }
      case "output" => period match {
        case "daily" => dailyOutputSchema
        case "weekly" => weeklyOutputSchema
        case "monthly" => monthlyOutputSchema
      }
    }

  }

}
