package com.extern.alphavantage

import com.commons.utils.DataFrameUtils.snakeCaseColumns
import com.commons.utils.HTTPHandler
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

class AlphaVantageClient(apiKey: String,
                         period: String,
                         outputSize: String) {

  val responseSchema: StructType = pickAlphaVantageSchema("response", period)

  def downloadDataToDataFrame(spark: SparkSession, ticker: String): DataFrame = {
    val response: String = downloadCSVData(ticker)
    transformResponseToDataFrame(spark, response)
      .transform(process(_, ticker))
  }

  def downloadCSVData(ticker: String): String = HTTPHandler.get(getUrl(ticker))

  def getUrl(ticker: String): String = {
    val apiFunction: String = period match {
      case "daily" => "TIME_SERIES_DAILY_ADJUSTED"
      case "weekly" => "TIME_SERIES_WEEKLY_ADJUSTED"
      case "monthly" => "TIME_SERIES_MONTHLY_ADJUSTED"
    }
    "%squery?function=%s&outputsize=%s&datatype=csv&symbol=%s&apikey=%s"
      .format(BASE_URL, apiFunction, outputSize, ticker, apiKey)
  }

  def transformResponseToDataFrame(spark: SparkSession, response: String): DataFrame = {
    import spark.implicits._
    val dataset: Dataset[String] = response.stripMargin.linesIterator.toList.toDS
    spark
      .read
      .schema(responseSchema)
      .option("header", "true")
      .csv(dataset)
      .toDF
  }

  def process(input: DataFrame, ticker: String): DataFrame = {
    input
      .withColumn("ticker", lit(ticker))
      .transform(snakeCaseColumns)
  }


}
