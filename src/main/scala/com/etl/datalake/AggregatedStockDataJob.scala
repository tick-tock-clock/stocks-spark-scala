package com.etl.datalake

import com.commons.traits.JobTraits
import com.commons.utils.{CustomConfig, CustomSparkSession, JobUtils, PartitionByDay}
import com.etl.datalake.StockDataJob.emptyStocks
import org.apache.spark.sql.expressions.{Window, WindowSpec}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.LongType
import org.apache.spark.sql.{DataFrame, SparkSession}

object AggregatedStockDataJob extends JobTraits {

  def main(args: Array[String]): Unit = {
    setupLocalLogger()
    timeJob {
      verifyArgs(args, "period", "years")
      val period: String = args(0)
      val years: Seq[Long] = args(1).split(",").map(_.toLong)

      val dates: Seq[String] = period match {
        case "weekly" =>
          years.map(JobUtils.getStartAndEndDateOfYear).toList.flatten
        case "monthly" =>
          years.map(year => List(s"$year-01-01", s"$year-12-31")).toList.flatten
      }

      logArgs(("period", period), ("years", years), ("startDt", dates.min), ("endDt", dates.max))

      val partitions: List[PartitionByDay] = PartitionByDay(dates.min).to(PartitionByDay(dates.max))

      val config: CustomConfig = CustomConfig("datalake", s"raw_stocks/$period")

      val spark: SparkSession = newSparkSession(getClass.getSimpleName)
      import CustomSparkSession.implicits._

      spark.writeDataFrameToDatabase(
        spark
          .readPartitionsByDayFromDatabase("datalake", s"raw_stocks/daily", partitions)
          .getOrElse(emptyStocks(spark))
          .transform(process(_, period)),
        config,
        "dt", "ticker"
      )
      spark.close()
    }
  }

  def process(input: DataFrame, period: String): DataFrame = {
    input.transform(addDateSpecifics).transform(aggregateOhlcToPeriod(_, period))
  }

  def addDateSpecifics(input: DataFrame): DataFrame = {
    import input.sparkSession.implicits._
    input
      .withColumn("year", year($"dt").cast(LongType))
      .withColumn("month", month($"dt").cast(LongType))
      .withColumn("week_of_year", weekofyear($"dt").cast(LongType))
      // Sometimes the first week starts at the previous year, then a window by year and week would
      // create two different groups instead of one
      .withColumn("adjusted_year", when($"month" === 12 && $"week_of_year" === 1, $"year" + 1)
        .otherwise($"year"))
  }

  def aggregateOhlcToPeriod(input: DataFrame, period: String): DataFrame = {
    import input.sparkSession.implicits._

    val windowSpec: WindowSpec = period match {
      case "weekly" => Window.partitionBy("ticker", "adjusted_year", "week_of_year")
      case "monthly" => Window.partitionBy("ticker", "year", "month")
    }

    val ascDtWindow: WindowSpec = windowSpec.orderBy($"dt".asc)
    val descDtWindow: WindowSpec = windowSpec.orderBy($"dt".desc)
    val maxHighWindow: WindowSpec = windowSpec.orderBy($"high".desc)
    val minLowWindow: WindowSpec = windowSpec.orderBy($"low".asc)

    input
      .withColumn("window_open", first("open").over(ascDtWindow))
      .withColumn("window_dt", first("dt").over(ascDtWindow))
      .withColumn("window_high", max("high").over(maxHighWindow))
      .withColumn("window_low", min("low").over(minLowWindow))
      .withColumn("window_close", first("close").over(descDtWindow))
      .select($"ticker",
        $"window_dt".as("dt"),
        $"window_open".as("open"),
        $"window_high".as("high"),
        $"window_low".as("low"),
        $"window_close".as("close")
      )
      .distinct
  }

}
