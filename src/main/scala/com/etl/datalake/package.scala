package com.etl

import org.apache.spark.sql.Column
import org.apache.spark.sql.functions.{col, greatest, least}
import org.apache.spark.sql.types.{DateType, DoubleType, LongType, StringType, StructType}

package object datalake {

  val stocksSchema:StructType= new StructType()
      .add("dt", DateType, nullable = false)
      .add("ticker", StringType, nullable = false)
      .add("open", DoubleType, nullable = false)
      .add("high", DoubleType, nullable = false)
      .add("low", DoubleType, nullable = false)
      .add("close", DoubleType, nullable = false)

  val usTradingHolidays: List[String] = List(
    "New Year's Day",
    "Martin Luther King, Jr. Day",
    "Washington's Birthday",
    "Memorial Day",
    "Independence Day",
    "Labour Day",
    "Thanksgiving Day",
    "Christmas Day",
  )

  val tradingHolidaysSchema: StructType = new StructType()
    .add("name", StringType)
    .add("dt", DateType)
    .add("country_code", StringType)

  val dateNagerInputSchema: StructType = new StructType()
    .add("name", StringType)
    .add("date", DateType)
    .add("countryCode", StringType)


  val preparedStocksSchema: StructType = {
    new StructType()
      .add("dt", DateType)
      .add("ticker", StringType)
      .add("open-2", DoubleType)
      .add("open-1", DoubleType)
      .add("open", DoubleType)
      .add("open+1", DoubleType)
      .add("open+2", DoubleType)
      .add("high-2", DoubleType)
      .add("high-1", DoubleType)
      .add("high", DoubleType)
      .add("high+1", DoubleType)
      .add("high+2", DoubleType)
      .add("low-2", DoubleType)
      .add("low-1", DoubleType)
      .add("low", DoubleType)
      .add("low+1", DoubleType)
      .add("low+2", DoubleType)
      .add("close-2", DoubleType)
      .add("close-1", DoubleType)
      .add("close", DoubleType)
      .add("close+1", DoubleType)
      .add("close+2", DoubleType)
  }

  val TICKER: Set[String] = Set(
    "1COV.DE",
    "AAD.DE",
    "AAPL",
    "ADJ.DE",
    "ADL.DE",
    "ADS.DE",
    "ADV.DE",
    "AFX.DE",
    "AIXA.DE",
    "ALV.DE",
    "AM3D.DE",
    "AMD",
    "AMGN",
    "AMZN",
    "ARL.DE",
    "ATVI",
    "AXP",
    "BA",
    "BABA",
    "BAC",
    "BAS.DE",
    "BAYN.DE",
    "BC8.DE",
    "BDT.DE",
    "BEI.DE",
    "BIDU",
    "BKNG",
    "BMSA.DE",
    "BMW.DE",
    "BOSS.DE",
    "BYND",
    "BYW6.DE",
    "C",
    "CAP.DE",
    "CAT",
    "COK.DE",
    "CON.DE",
    "COP.DE",
    "COST",
    "CWC.DE",
    "DAI.DE",
    "DB1.DE",
    "DBK.DE",
    "DDD",
    "DEQ.DE",
    "DIA",
    "DIC.DE",
    "DLG.DE",
    "DPW.DE",
    "DRI.DE",
    "DRW3.DE",
    "DTE.DE",
    "DUE.DE",
    "EOAN.DE",
    "EVD.DE",
    "EVK.DE",
    "EVT.DE",
    "F",
    "FB",
    "FCX",
    "FIE.DE",
    "FME.DE",
    "FNTN.DE",
    "FPE3.DE",
    "FRA.DE",
    "FRE.DE",
    "FRU.DE",
    "FSLR",
    "G24.DE",
    "GFK.DE",
    "GFT.DE",
    "GIL.DE",
    "GM",
    "GMM.DE",
    "GOLD",
    "GOOG",
    "GS",
    "GXI.DE",
    "HAB.DE", // might be not available
    "HBH.DE",
    "HD",
    "HEI.DE",
    "HEN3.DE",
    "HHFA.DE",
    "HNR1.DE",
    "HOT.DE",
    "HPQ",
    "HYQ.DE",
    "IBM",
    "IFX.DE",
    "INTC",
    "JEN.DE",
    "JPM",
    "JUN3.DE",
    "KCO.DE",
    "KGX.DE",
    "KHC",
    "KRN.DE",
    "KWS.DE",
    "LEG.DE",
    "LHA.DE",
    "LIN.DE",
    "LXS.DE",
    "MCD",
    "MEO.DE",
    "MJ",
    "MNST",
    "MOR.DE",
    "MRK.DE",
    "MSFT",
    "MU",
    "MUV2.DE",
    "NDX1.DE",
    "NEM.DE",
    "NFLX",
    "NKE",
    "NOEJ.DE",
    "NVDA",
    "O1BC.DE",
    "O2D.DE",
    "OSR.DE",
    "P1Z.DE",
    "PFE",
    "PFV.DE",
    "PUM.DE",
    "PYPL",
    "QCOM",
    "QIA.DE",
    "QQQ",
    "RHK.DE",
    "RIB.DE",
    "RRTL.DE",
    "RWE.DE",
    "S92.DE",
    "SAP.DE",
    "SAZ.DE",
    "SBS.DE",
    "SBUX",
    "SFQ.DE",
    "SGL.DE",
    "SHA.DE",
    "SIE.DE",
    "SMHN.DE",
    "SNAP",
    "SNH.DE",
    "SOW.DE",
    "SPY",
    "SRT3.DE",
    "STM.DE",
    "SZU.DE",
    "T",
    "TEG.DE",
    "TIM.DE",
    "TKA.DE",
    "TLG.DE",
    "TLRY",
    "TSLA",
    "TTK.DE",
    "TWTR",
    "UAA",
    "UAL",
    "UBER",
    "UTDI.DE",
    "V",
    "VNA.DE",
    "VOW3.DE",
    "WAF.DE",
    "WDI.DE",
    "WFC",
    "WMT",
    "WSU.DE",
    "WUW.DE",
    "WYNN",
    "XBI",
    "XLC",
    "XLE",
    "XLF",
    "XLI",
    "XLK",
    "XLP",
    "XLU",
    "XLV",
    "XLY",
    "XOM",
    "ZAL.DE",
    "NKLA",
    "NIO",
    "ZM")
  val TICKER_LENGTH: Int = TICKER.toList.length
}
