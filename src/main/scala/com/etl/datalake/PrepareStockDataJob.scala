package com.etl.datalake

import com.commons.traits.JobTraits
import com.commons.utils.DataFrameUtils.selectWithSchema
import com.commons.utils.{CustomConfig, CustomSparkSession, PartitionByDay}
import org.apache.spark.sql.expressions.{Window, WindowSpec}
import org.apache.spark.sql.functions.{col, lead}
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

object PrepareStockDataJob extends JobTraits {

  /**
   * Args =>
   * period(daily, weekly, monthly)
   * startDt(yyyy-mm-dd)
   * endDt(yyyy-mm-dd)
   */
  def main(args: Array[String]): Unit = {
    setupLocalLogger()
    timeJob {

      verifyArgs(args, "period", "startDt", "endDt")
      val period: String = args(0)
      val startDt: String = args(1)
      val endDt: String = args(2)

      val partitions: List[PartitionByDay] = PartitionByDay(startDt).to(PartitionByDay(endDt))

      logArgs(("period", period), ("startDt", startDt), ("endDt", endDt), ("partitions", partitions))
      val config: CustomConfig = CustomConfig("datalake", s"prepared_stocks/$period")
      val spark: SparkSession = newSparkSession()
      import CustomSparkSession.implicits._


      val maybeDataFrame: Option[DataFrame] = spark
        .readPartitionsByDayFromDatabase("datalake", s"raw_stocks/$period", partitions)

      maybeDataFrame match {
        case None => logger.warn("No input path found, exiting successfully")
        case Some(stocks) =>
          spark.writeDataFrameToDatabase(
            stocks
              .transform(shiftPricesForKeyColumns(_, "open", "high", "low", "close"))
              .transform(selectWithSchema(_, preparedStocksSchema)),
            config,
            "dt", "ticker"
          )
      }
      spark.close()
    }
  }

  def shiftPricesForKeyColumns(input: DataFrame, columns: String*): DataFrame = {
    columns.foldLeft(input) { (memoDF, column) =>
      memoDF.transform(shiftPricesAndAppendAsColumn(_, column))
    }.na.drop("any")
  }


  def shiftPricesAndAppendAsColumn(input: DataFrame, column: String): DataFrame = {
    List[String]("-2", "-1", "0", "+1", "+2").foldLeft(input) { (memoDF, shift) =>
      val window: WindowSpec = Window.partitionBy("ticker").orderBy("dt")
      val leadColumn: Column = lead(col(column), shift.toInt).over(window)
      memoDF.withColumn(s"$column$shift", leadColumn)
    }.drop(column).withColumnRenamed(s"${column}0", column)
  }

}
