package com.etl.datalake

import com.commons.traits.JobTraits
import com.commons.utils.{CustomConfig, CustomSparkSession, HTTPHandler}
import org.apache.spark.sql.{DataFrame, SparkSession}

object TradingHolidaysJob extends JobTraits {

  val DATE_NAGER_BASE_URL: String = "https://date.nager.at/api/v2/PublicHolidays"

  /**
   * Intends to fetch all trading holidays in the US for a Sequence of given years and
   * saves them to datalake. Can then be used to determine trading days excluding
   * the holidays.
   * Fetched from Nager.Date API
   *
   * @param args
   * years => comma separated list of years
   */
  def main(args: Array[String]): Unit = {
    setupLocalLogger()
    timeJob {
      verifyArgs(args, "years")
      val years: Seq[String] = args(0).split(",").toSeq
      logArgs(("years", years))
      val config: CustomConfig = CustomConfig("datalake", "trading_holidays")

      val spark: SparkSession = newSparkSession()
      import CustomSparkSession.implicits._
      val output = years.map(year => {
        logger.warn(s"Requesting Holidays for $year")
        val response = HTTPHandler.get(s"$DATE_NAGER_BASE_URL/$year/US")
        responseToDataFrame(spark, response)
      }).reduce(_ union _)

      spark.writeDataFrameToDatabase(output, config, "dt")

      spark.close()

    }
  }

  def responseToDataFrame(spark: SparkSession, response: String): DataFrame = {
    import spark.implicits._
    spark.read.schema(dateNagerInputSchema).json(Seq(response).toDS)
      .filter($"name".isin(usTradingHolidays: _*))
      .withColumnRenamed("date", "dt")
      .withColumnRenamed("countryCode", "country_code")

  }

}
