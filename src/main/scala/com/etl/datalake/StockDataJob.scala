package com.etl.datalake

import com.commons.traits.JobTraits
import com.commons.utils.DataFrameUtils.{emptyDataFrame, selectWithSchema}
import com.commons.utils.{CustomConfig, CustomSparkSession}
import com.extern.alphavantage.{AlphaVantageClient, pickAlphaVantageSchema}
import org.apache.spark.sql.expressions.{Window, WindowSpec}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{DoubleType, LongType, StructType}
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

/**
 * In Production we should always pull full history because compact data would not adjust
 * historical data with split_coefficient
 *
 * Args =>
 * outputTable(stocks)
 * period(daily, weekly, monthly)
 * output size(full, compact)
 */
object StockDataJob extends JobTraits {
  def main(args: Array[String]): Unit = {
    setupLocalLogger()
    timeJob {
      verifyArgs(args, "period", "outputSize")
      val period: String = args(0)
      val outputSize: String = args(1)
      logArgs(("period", period), ("outputSize", outputSize))

      val config: CustomConfig = CustomConfig("datalake", s"raw_stocks/$period")

      val apiKey: String = config.getSecretsConfig.getString("api.alpha_vantage.key")
      val outputSchema: StructType = pickAlphaVantageSchema("output", period)
      val alphaVantageClient = new AlphaVantageClient(apiKey, period, outputSize)

      val spark: SparkSession = newSparkSession(getClass.getSimpleName)
      import CustomSparkSession.implicits._

      TICKER.zipWithIndex.foreach({ case (ticker, index) =>
        logger.warn(s"Downloading Ticker => $ticker | ${index + 1}/${TICKER_LENGTH + 1}")

        var output: DataFrame = alphaVantageClient
          .downloadDataToDataFrame(spark, ticker)
          .withColumn("dt", date_format(col("timestamp"), "yyyy-MM-dd"))

        output = period match {
          case "daily" => output.transform(
            adjustStockSplit(_, Seq("open", "high", "low", "close", "dividend_amount", "volume")))
          case _ => output
        }

        spark.writeDataFrameToDatabase(
          output.transform(selectWithSchema(_, outputSchema)),
          config,
          "dt", "ticker")
      })

      spark.close()
    }
  }

  def adjustStockSplit(input: DataFrame, columnsToAdjust: Seq[String]): DataFrame = {

    columnsToAdjust.foldLeft(input) { (memoDF, column) =>
      val window: WindowSpec = Window.partitionBy("ticker").orderBy(desc("dt"))
      val lagColumn: Column = lag(col("split_coefficient"), 1).over(window)
      val cumProductColumn: Column = round(exp(sum(log(col("lag"))).over(window)), 2)
      val tempOutput = memoDF
        .withColumn("lag", lagColumn)
        .na.fill(1D)
        .withColumn("cum_split", cumProductColumn)

      column match {
        case "volume" =>
          tempOutput.withColumn(column, round(col(column).multiply(col("cum_split")))
            .cast(LongType))
        case _ =>
          tempOutput.withColumn(column, round(col(column).divide(col("cum_split")), 3)
            .cast(DoubleType))
      }

    }


  }

  def emptyStocks(spark: SparkSession): DataFrame = emptyDataFrame(spark, stocksSchema)

}
