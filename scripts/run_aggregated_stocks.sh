#!/bin/bash
set -e

# Run weekly
compile-and-run-locally com.etl.datalake.AggregatedStockDataJob weekly \
  '1999,2000,2001,2002' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob weekly \
    '2003,2004,2005' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob weekly \
    '2006,2007,2008' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob weekly \
    '2009,2010' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob weekly \
    '2011,2012,2013' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob weekly \
    '2014,2015' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob weekly \
    '2016,2017,2018' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob weekly \
    '2019,2020,2021'

# Run monthly
compile-and-run-locally com.etl.datalake.AggregatedStockDataJob monthly \
  '1999,2000,2001,2002' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob monthly \
    '2003,2004,2005' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob monthly \
    '2006,2007,2008' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob monthly \
    '2009,2010' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob monthly \
    '2011,2012,2013' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob monthly \
    '2014,2015' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob monthly \
    '2016,2017,2018' &&
  compile-and-run-locally com.etl.datalake.AggregatedStockDataJob monthly \
    '2019,2020,2021'
