#!/bin/bash
set -e

TODAY=$(printf '%(%Y-%m-%d)T\n' -1)

compile-and-run-locally com.etl.datalake.PrepareStockDataJob daily 1999-11-01 2006-01-10 &&
  compile-and-run-locally com.etl.datalake.PrepareStockDataJob daily 2005-12-25 2010-01-10 &&
  compile-and-run-locally com.etl.datalake.PrepareStockDataJob daily 2009-12-25 2014-01-10 &&
  compile-and-run-locally com.etl.datalake.PrepareStockDataJob daily 2013-12-25 2016-01-10 &&
  compile-and-run-locally com.etl.datalake.PrepareStockDataJob daily 2015-12-25 2017-01-10 &&
  compile-and-run-locally com.etl.datalake.PrepareStockDataJob daily 2016-12-25 2019-01-10 &&
  compile-and-run-locally com.etl.datalake.PrepareStockDataJob daily 2018-12-25 2021-01-10 &&
  compile-and-run-locally com.etl.datalake.PrepareStockDataJob daily 2020-12-25 $TODAY
